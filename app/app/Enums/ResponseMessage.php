<?php


namespace App\Enums;


interface ResponseMessage
{
    const SUCCESSFULLY_STORED = "SUCCESSFULLY_STORED";
    const INVALID_DATA = "INVALID_DATA";
    const SUCCESSFULLY_UPDATED = "SUCCESSFULLY_UPDATED";
}

# Telenor Coding Test


## Run application

### With docker
At first copy docker-compose.yaml.sample to docker-compose.yaml
```$xslt
cp docker-compose.yaml.sample docker-compose.yaml
```
then  go to app/ folder
```
cd app
```
then copy .env.example to .env
```
cp .env.example .env
``` 
now back to root folder. you need to symblink app/.env file here, so
```$xslt
ln -s /telenor-folder-path/app/.env /telenor-folder-path/
```
then need to build the images and up container
```$xslt
docker-compose up --build 
```
if everything is properly start and if you don't change the APP_PORT on app/.env
then you can access this app on [http://localhost:8051](http://localhost:8051).   

Then run this command for download packages

```$xslt
docker-composer exec app composer install
```
Then set database configuration on .env. If everything is proper then migrate database
```$xslt
docker-compose exec app php artisan nigrate
```




### Without docker
At first go to app/ folder
```
cd app
```
Then copy .env.example to .env
```
cp .env.example .env
```
Then run this command for download packages

```$xslt
composer install
```
Then set database configuration on .env. If everything is proper then migrate database
```$xslt
php artisan migrate
```
Then start php server for run this application

```$xslt
php artisan serve
```
if everything is ok then app will start on [http://localhost:8000](http://localhost:8000)





## Application Index

### Storage
Use mysql database for store values.

### Routes
All application routes are stored on app/routes/web.php.

### Controller
Use KVController.php on app/app/Http/Controllers/KVController.php.

### TTL
Use 5 min / 300 seconds expiry time for each data. You can change the value of "CACHE_TTL" of app/.env.

### Test case
Try to writing some test case on app/tests/Feature/KvTest.php. You can try run those test case
```$xslt
./vendor/bin/phpunit
```
### Postman Collection
Here is also an exported file "Telenor.postman_collection.json". It will help you to test this application api's.
You can check also [web version postman collection](https://documenter.getpostman.com/view/391183/SWLZfVSF).
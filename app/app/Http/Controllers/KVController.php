<?php

namespace App\Http\Controllers;

use App\Enums\ResponseMessage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class KVController extends Controller
{
    /**
     * Retrieve stored data and
     * update TTL in every
     * GET request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getValues(Request $request)
    {
        if (!$request->filled('keys')) {
            $cachedKV = DB::table('cache')->select('key')->get()->pluck('key');
        } else {
            $cachedKV = explode(',', $request->input('keys'));
        }
        $prefix= Cache::getPrefix();
        $data = [];

        foreach($cachedKV as $kv) {
            $key = str_replace($prefix, '', $kv);
            $value = Cache::get($key);
            if (!blank($value)) {
                $data[$key] = $value;
                Cache::put($key, $value, env('CACHE_TTL'));
            }
        }

        return response()->json($data, Response::HTTP_OK);

    }

    /**
     * Store data and update TTL of
     * existing data
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeKeyValue(Request $request)
    {
        $getRequestedKV = $request->all();
        $validationMethod = [
            'array',
            'assoc',
            'valid_key',
            'not_blank'
        ];

        if ($this->verify($getRequestedKV, $validationMethod)) {
            foreach($getRequestedKV as $key => $value) {
                if (Cache::has($key)) {
                    Cache::put($key, $value, env('CACHE_TTL'));
                } else {
                    Cache::add($key, $value, env('CACHE_TTL'));
                }
            }

            return response()->json([
                'message' => ResponseMessage::SUCCESSFULLY_STORED
            ], Response::HTTP_CREATED);

        } else {

            return response()->json([
                'message' => ResponseMessage::INVALID_DATA
            ], Response::HTTP_UNPROCESSABLE_ENTITY);

        }

    }

    /**
     * Update key value and store
     * also new value
     * Retriev
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateKeyValue(Request $request)
    {
        $getRequestedKV = $request->all();
        $validationMethod = [
            'array',
            'assoc',
            'valid_key',
            'not_blank'
        ];

        if ($this->verify($getRequestedKV, $validationMethod)) {
            foreach($getRequestedKV as $key => $value) {
                Cache::put($key, $value, env('CACHE_TTL'));
            }

            return response()->json([
                'message' => ResponseMessage::SUCCESSFULLY_UPDATED
            ], Response::HTTP_OK);

        } else {

            return response()->json([
                'message' => ResponseMessage::INVALID_DATA
            ], Response::HTTP_UNPROCESSABLE_ENTITY);

        }
    }

    /**
     * Verify requested data by
     * specific method list
     *
     * @param array $data
     * @param array $validation
     * @return bool|mixed
     */
    private function verify(array $data, $validation = [])
    {
        $pass = true;

        foreach ($validation as $method) {
            $method = Str::camel(sprintf('is_%s', $method));
            $pass &= call_user_func_array([$this, $method], [$data]);
        }

        return $pass;
    }

    /**
     * Check array blank or not
     *
     * @param array $array
     * @return bool
     */
    private function isNotBlank(array $array)
    {
        return !blank($array);
    }

    /**
     * Check given data array or not
     *
     * @param array $array
     * @return bool
     */
    private function isArray(array $array)
    {
        return is_array($array);
    }

    /**
     * Check array keys are valid or not
     *
     * @param array $array
     * @return bool
     */
    private function isValidKey(array $array)
    {
        $error = 0;

        foreach($array as $key => $value) {
            if (blank($key) || empty($key)) {
                $error++;
            }
        }

        return (!$error ? true : false);
    }

    /**
     * Check array associative or not
     *
     * @param array $array
     * @return bool
     */
    private function isAssoc(array $array)
    {
        return Arr::isAssoc($array);
    }
}

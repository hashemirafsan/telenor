<?php

namespace Tests\Feature;

use App\Enums\ResponseMessage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Response;
use Tests\TestCase;

class KvTest extends TestCase
{
    /**
     * Test "/values" @post route
     * Trying to store data
     */
    public function testStoreRoute()
    {
        $response = $this->json('POST', '/values', ['key1' => 'Hashemi', 'key2' => 'Rafsan']);

        $response->assertStatus(\Illuminate\Http\Response::HTTP_CREATED)
                 ->assertExactJson([
                     'message' => ResponseMessage::SUCCESSFULLY_STORED
                 ]);
    }

    /**
     * Test "/values" @post route
     * Given invalid data and trying to submit
     * 1st attempt
     */
    public function testStoreRouteFaultValueCaseOne()
    {
        $response = $this->post('/values');

        $response->assertStatus(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertExactJson([
                'message' => ResponseMessage::INVALID_DATA
            ]);
    }

    /**
     * Test "/values" @post route
     * Given invalid data and trying to submit
     * 2nd attempt
     */
    public function testStoreRouteFaultValueCaseTwo()
    {
        $response = $this->post('/values', []);

        $response->assertStatus(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertExactJson([
                'message' => ResponseMessage::INVALID_DATA
            ]);
    }

    /**
     * Test "/values" @post route
     * Given invalid data and trying to submit
     * 3rd attempt
     */
    public function testStoreRouteFaultValueCaseThree()
    {
        $response = $this->post('/values', [1,2,3,4,5]);
        $response->assertStatus(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertExactJson([
                'message' => ResponseMessage::INVALID_DATA
            ]);
    }

    /**
     * Test "/values" @post route
     * Given invalid data and trying to submit
     * 4th attempt
     */
    public function testStoreRouteFaultValueCaseFour()
    {
        $response = $this->post('/values', ['', '', 'rafsan' => 1]);
        $response->assertStatus(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertExactJson([
                'message' => ResponseMessage::INVALID_DATA
            ]);
    }

    /**
     * Test "/values" @get route
     * Retrieving store data
     */
    public function testGetStoreRoute()
    {
       $response = $this->get('/values');
       $response->assertStatus(\Illuminate\Http\Response::HTTP_OK)
                ->assertJson([
                    'key1' => 'Hashemi',
                    'key2' => 'Rafsan'
                ]);
    }

    /**with
     * Test "/values" @get route
     * Retrieving store data by specific keys
     */
    public function testGetStoreRouteWithSpecificValue()
    {
        $response = $this->get('/values?keys=key1');
        $response->assertStatus(\Illuminate\Http\Response::HTTP_OK)
            ->assertExactJson([
                'key1' => 'Hashemi'
            ]);
    }

    /**
     * Test "/values" @patch route
     * Trying to update existing value or store new item
     */
    public function testUpdateRoute()
    {
        $response = $this->json('PATCH', '/values', ['key1' => 'Rafsan']);
        $response->assertStatus(\Illuminate\Http\Response::HTTP_OK)
            ->assertExactJson([
                'message' => ResponseMessage::SUCCESSFULLY_UPDATED
            ]);
    }

    /**
     * Test "/values" @patch route
     * Given invalid data and trying to submit
     * 1st attempt
     */
    public function testUpdateRouteFaultValueCaseOne()
    {
        $response = $this->patch('/values');

        $response->assertStatus(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertExactJson([
                'message' => ResponseMessage::INVALID_DATA
            ]);
    }

    /**
     * Test "/values" @patch route
     * Given invalid data and trying to submit
     * 2nd attempt
     */
    public function testUpdateRouteFaultValueCaseTwo()
    {
        $response = $this->patch('/values', []);

        $response->assertStatus(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertExactJson([
                'message' => ResponseMessage::INVALID_DATA
            ]);
    }

    /**
     * Test "/values" @patch route
     * Given invalid data and trying to submit
     * 3rd attempt
     */
    public function testUpdateRouteFaultValueCaseThree()
    {
        $response = $this->patch('/values', [1,2,3,4,5]);
        $response->assertStatus(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertExactJson([
                'message' => ResponseMessage::INVALID_DATA
            ]);
    }

    /**
     * Test "/values" @patch route
     * Given invalid data and trying to submit
     * 4th attempt
     */
    public function testUpdateRouteFaultValueCaseFour()
    {
        $response = $this->patch('/values', ['', '', 'rafsan' => 1]);
        $response->assertStatus(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertExactJson([
                'message' => ResponseMessage::INVALID_DATA
            ]);
    }

}
